const origen = document.getElementById("origen");
const destino = document.getElementById("destino");
const calcular = document.getElementById("btnCalcular");

//evento change
origen.addEventListener("change", function () {
    const selectedValue = origen.value;
    const options = destino.options;
    for (let i = 0; i < options.length; i++) {
        if (options[i].value === selectedValue) {
            options[i].style.display = "none";
        } else {
            options[i].style.display = "";
        }
    }
});

// funcion calcular

calcular.addEventListener("click", function () {

    const cantidad = parseFloat(document.getElementById("cantidad").value);
    const origen = parseInt(document.getElementById("origen").value);
    const destino = parseInt(document.getElementById("destino").value);

    //dar taza de precios
    const preciomexicano = 19.85;
    const precioestaunidense = 1;
    const preciocanadiense = 1.35;
    const precioeuro = 0.99;

    let subtotal;
    //calculos
    switch (origen) {
        case 1: // Dólar Estadounidense
            switch (destino) {
                case 1: // Dólar Estadounidense
                    subtotal = cantidad * precioestaunidense;
                    break;
                case 2: // Peso Mexicano
                    subtotal = cantidad * preciomexicano;
                    break;
                case 3: // Dólar Canadiense
                    subtotal = cantidad * preciocanadiense;
                    break;
                case 4: // Euro
                    subtotal = cantidad * precioeuro;
                    break;
            }
            break;
        case 2: // Peso Mexicano
            switch (destino) {
                case 1: // Dólar Estadounidense
                    subtotal = cantidad / preciomexicano;
                    break;
                case 2: // Peso Mexicano
                    subtotal = cantidad * precioestaunidense / preciomexicano;
                    break;
                case 3: // Dólar Canadiense
                    subtotal = cantidad / preciomexicano * preciocanadiense;
                    break;
                case 4: // Euro
                    subtotal = cantidad / preciomexicano * precioeuro;
                    break;
            }
            break;
        case 3: // Dólar Canadiense
            switch (destino) {
                case 1: // Dólar Estadounidense
                    subtotal = cantidad / preciocanadiense;
                    break;
                case 2: // Peso Mexicano
                    subtotal = cantidad / preciocanadiense * preciomexicano;
                    break;
                case 3: // Dólar Canadiense
                    subtotal = cantidad * precioestaunidense;
                    break;
                case 4: // Euro
                    subtotal = cantidad / preciocanadiense * precioeuro;
                    break;
            }
            break;
        case 4: // Euro
            switch (destino) {
                case 1: // Dólar Estadounidense
                    subtotal = cantidad / precioeuro;
                    break;
                case 2: // Peso Mexicano
                    subtotal = cantidad / precioeuro * preciomexicano;
                    break;
                case 3: // Dólar Canadiense
                    subtotal = cantidad / precioeuro * preciocanadiense;
                    break;
                case 4: //Euro
                    subtotal = cantidad * precioestaunidense / precioeuro;
                    break;
            }
            break;

    }

    const comision = subtotal * 0.03;
    const total = subtotal + comision;

    document.getElementById("subtotal").value = subtotal.toFixed(2);
    document.getElementById("comision").value = comision.toFixed(2);
    document.getElementById("totalPagar").value = total.toFixed(2);
});

//funcion agregar registro
function agregarRegistro() {
    const cantidad = parseInt(document.getElementById("cantidad").value);
    const origen = parseInt(document.getElementById("origen").value);
    const destino = parseInt(document.getElementById("destino").value);
    const comision   = parseFloat(document.getElementById("comision").value);
    const subtotal = parseFloat(document.getElementById("subtotal").value);
    const totalPagar = parseFloat(document.getElementById("totalPagar").value);

    let origenTxt;
    let destinoTxt;
  
    //txts de los paises
    switch (origen) {
        case 1:
            origenTxt = "Dólar Estadounidense";
            break;
        case 2:
            origenTxt = "Pesos Mexicanos";
            break;
        case 3:
            origenTxt = "Dólar Canadiense";
            break;
        case 4:
            origenTxt = "Euro";
            break;
    }
    switch (destino) {
        case 1:
            destinoTxt = "Dólar Estadounidense";
            break;
        case 2:
            destinoTxt = "Pesos Mexicanos";
            break;
        case 3:
            destinoTxt = "Dólar Canadiense";
            break;
        case 4:
            destinoTxt = "Euro";
            break;
    }
    const tabla = document.getElementById("registros");
    const newRow = `
        <tr>
          <td>${cantidad}</td>
          <td>${origenTxt}</td>
          <td>${destinoTxt}</td>
          <td>${subtotal.toFixed(2)}</td>
          <td>${comision.toFixed(2)}</td>
          <td>${totalPagar.toFixed(2)}</td>
        </tr>
      `;

    tabla.innerHTML += newRow;

    let totalGeneral = 0;
    const filas = tabla.getElementsByTagName("tr");

    for (let i = 1; i < filas.length; i++) {
        const totalPagar = parseFloat(filas[i].getElementsByTagName("td")[5].textContent);
        totalGeneral += totalPagar;
    }

    document.getElementById("total").textContent = "$" + totalGeneral.toFixed(2);
}

//funcion eliminar registro
function eliminarRegistro() {
    const tabla = document.getElementById("registros");
    let numRows = tabla.rows.length;
    for (let i = numRows - 1; i > 0; i--) {
        tabla.deleteRow(i);
    }
    let totalGeneral = 0;
    document.getElementById("total").textContent = totalGeneral;
}

